package com.sda.test;

import java.util.Map;
import java.util.HashMap;

public class MAin {
    public static void main(String[] args) {

        Student student1 = new Student(100100, "Marian", "Paździoch");
        Student student2 = new Student(100200, "Joanna", "Kowalska");
        Student student3 = new Student(100300, "Karolina", "Kiepska");
        Student student4 = new Student(100400, "Andrzej", "Kowalski");

        Map<Long,Student> mapaStudentow = new HashMap<>();
        mapaStudentow.put(student1.numerIndeksu,student1);
        mapaStudentow.put(student1.getNumerIndeksu(),student1);
        mapaStudentow.put(student2.getNumerIndeksu(),student2);
        mapaStudentow.put(student3.getNumerIndeksu(),student3);
        mapaStudentow.put(student4.getNumerIndeksu(),student4);

        System.out.println(mapaStudentow.keySet());


    }
}
